using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    Rigidbody rb;

    [SerializeField]
    private float speedValue = 1f;

    [SerializeField]
    private LayerMask groundLayer;

    private bool isGrounded = false;

    private float speed;

    public float Speed 
    {
        get
        {
            return speed;
        }
        set
        {
            speed = value >= 0 ? value : 0;
        }
    }

    public Transform BootsPos;
    public Transform meshBootsPos1;
    public Transform meshBootsPos2;

    public InputMesh InputMesh;

    private float hitAngle;

    Vector3 gravity = Vector3.zero;
    private Mesh mesh;

    private MeshCollider meshCollider;

    private Effector effectingObj;

    // Start is called before the first frame update
    void Start()
    {
        speed = speedValue;

        rb = transform.GetComponent<Rigidbody>();
        meshCollider = meshBootsPos1.GetComponent<MeshCollider>();

        InputMesh.OnMeshChanged += InputMesh_OnMeshChanged;

    }

#if UNITY_EDITOR

    private void OnValidate()
    {
        if (Application.isPlaying)
        {
            speed = speedValue;
        }
    }
#endif


    private void OnDestroy()
    {
        InputMesh.OnMeshChanged -= InputMesh_OnMeshChanged;
    }

    private void InputMesh_OnMeshChanged()
    {
        float bootsPositionY = meshBootsPos1.localPosition.y /4;
        BootsPos.localPosition = new Vector3(BootsPos.localPosition.x, bootsPositionY , BootsPos.localPosition.z);

        transform.position -= Vector3.up * bootsPositionY;
        mesh = meshBootsPos1.GetComponent<MeshFilter>().mesh;

        if (effectingObj != null)
        {
            effectingObj.StopEffect(gameObject);
            effectingObj = null;
        }

    }

    private void FixedUpdate()
    {
        if (rb != null)
        {
            Vector3 pos = Vector3.zero;
            
            RaycastHit hit;

             isGrounded = false;
            if (Physics.CapsuleCast(BootsPos.position, BootsPos.position - Vector3.up * 0.0001f, 0.01f, -BootsPos.up, out hit, 0.1f, groundLayer))
            {
                isGrounded = CheckIsGrounded(hit);

                Vector3 vec = (BootsPos.position - hit.point).normalized;

                pos = vec;

                if(isGrounded == true)
                {
                    gravity = Vector3.zero;
                }

            }
            pos += Vector3.right * speed;

            if (!isGrounded && rb.useGravity)
            {
                //pos += Physics.gravity * Time.fixedDeltaTime;
                gravity += Physics.gravity * Time.fixedDeltaTime;
            }
            Debug.Log(pos);
            //rb.MovePosition(transform.position + pos * Time.fixedDeltaTime);
            rb.MovePosition(Vector3.Lerp(transform.position, transform.position + pos + gravity, speed * Time.fixedDeltaTime));
            return;
        }
    }

    private bool CheckIsGrounded(RaycastHit hit)
    {
        Vector3 colideVec =  Vector3.down;
        float colideAngle = Vector3.Angle(Vector3.down, (hit.point - BootsPos.position).normalized);

        Debug.Log(Vector3.Dot(hit.normal, Vector3.up));

        hitAngle = colideAngle;

        return colideAngle < 90;

    }

    private void ChangeSpeed()
    {
        int co = mesh.vertices.Length / 2;
       // meshCollider.poi
    }

    private void OnCollisionEnter(Collision collision)
    {
        Effector effector = collision.gameObject.GetComponent<Effector>();

        if(!effector)
        {
            return;
        }

        if(effectingObj == null)
        {
            effectingObj = effector;
            effector.Effect(gameObject);
        }

        if (effectingObj != null && effector.gameObject != effectingObj.gameObject)
        {
            effectingObj.StopEffect(gameObject);
            effectingObj = effector;
            effector.Effect(gameObject);

        }

        Debug.Log("Contacts = " + collision.contacts.Length + "Speed = " + speed);

    }



    public void ResetSpeed()
    {
        speed = speedValue;
    }
}
