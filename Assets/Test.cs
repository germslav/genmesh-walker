using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public GameObject sphere;

    private List<GameObject> gameObjects = new List<GameObject>();

    public bool isGen = false;

    InputMesh g;
    void Start()
    {
        g = GetComponent<InputMesh>();
    }


#if UNITY_EDITOR
    private void OnValidate()
    {
        if (Application.isPlaying)
        {
            isGen = false;
            foreach(GameObject obj in gameObjects)
            {
                Destroy(obj);
            }

            gameObjects.Clear();
        }
    }
#endif

    // Update is called once per frame
    void Update()
    {
        
        if(g.verticiesList.Count <= 0 || isGen)
        {
            return;
        }

        isGen = true;
        for (int i = 0; i < g.verticiesList.Count; i++)
        {
            var sps = Instantiate(sphere, transform);
            sps.transform.position = transform.position + g.verticiesList[i];
            //sps.transform.localScale /= 2;
            sps.name =  (i).ToString();
            sps.SetActive(true);
            gameObjects.Add(sps);


        }

        for (int i = 0; i < g.touchPointsList.Count; i++)
        {
            var sps = Instantiate(sphere, transform);
            sps.transform.position = transform.position + g.touchPointsList[i];
            sps.transform.localScale *= 2;
            sps.name = "Point " + (i).ToString();
            sps.SetActive(true);
            gameObjects.Add(sps);


        }
    }
}
