using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter), typeof(MeshCollider))]
public class InputMesh : MonoBehaviour
{

    [SerializeField]
    private GameObject MeshContainer1;
    [SerializeField]
    private GameObject MeshContainer2;

    public float thickness = 1;
    public float distBtwMouseClick = 250;
    public float distBtwPoints = 1;

    public float zThickness = 70;

    public float sdvigY = 2f;

    public List<Vector3> touchPointsList = new List<Vector3>();

    private List<Vector3> touchPointsListTmp = new List<Vector3>();

    private Vector3 prevMousePos;

    public List<Vector3> verticiesList = new List<Vector3>();
    List<int> trianglesList = new List<int>();
    List<Vector3> normalsList = new List<Vector3>();

    private MeshFilter meshFilter1;
    private MeshRenderer meshRenderer1;
    private MeshCollider meshCollider1;

    private MeshFilter meshFilter2;
    private MeshRenderer meshRenderer2;
    private MeshCollider meshCollider2;

    private Transform bootTransform1;
    private Transform bootTransform2;

    private Vector3 maxVertexPos;


    public delegate void OnMeshChangedHandler();
    public event OnMeshChangedHandler OnMeshChanged;


    bool isDebug = false;
    bool isSwipe = false;
    Vector3 prevTouchPos;

    private int pointsCount
    {
        get
        {
            return touchPointsList.Count;
        }
    }


#if UNITY_EDITOR

    private void OnValidate()
    {
        if (Application.isPlaying && touchPointsList.Count > 0)
        {
            GenerateMesh();
        }
    }
#endif

    private void Awake()
    {
        meshFilter1 = MeshContainer1.GetComponent<MeshFilter>();
        meshRenderer1 = MeshContainer1.GetComponent<MeshRenderer>();
        meshCollider1 = MeshContainer1.GetComponent<MeshCollider>();

        meshFilter2 = MeshContainer2.GetComponent<MeshFilter>();
        meshRenderer2 = MeshContainer2.GetComponent<MeshRenderer>();
        meshCollider2 = MeshContainer2.GetComponent<MeshCollider>();

        bootTransform1 = MeshContainer1.transform.parent.transform;
        //bootTransform2 = MeshContainer2.transform.parent.transform;

    }

    void Start()
    {
        //Debug.Log(testVec.Length);
        //TestMesh();
    }


    private void GetTouch()
    {
        int touchCount = Input.touchCount;

        if (touchCount > 0)
        {

            Touch touch = Input.GetTouch(0);


            if (!isSwipe)
            {
                isSwipe = true;
                touchPointsListTmp.Clear();

                Vector3 point = touch.position;
                point.z *= 0;
                prevTouchPos = point;
                touchPointsListTmp.Add(point);

                return;
            }

            Vector3 dir = (Vector3)touch.position - prevTouchPos;
            float mag = dir.magnitude;

            if (mag >= distBtwMouseClick)
            {
                prevTouchPos = touch.position;

                Vector3 point = prevTouchPos;
                point.z *= 0;

                touchPointsListTmp.Add(point);

            }


            isSwipe = !touch.phase.HasFlag(TouchPhase.Ended);

            if(!isSwipe)
            {
                if (touchPointsListTmp.Count < 6)
                {
                    return;
                }

                touchPointsList = touchPointsListTmp;
                touchPointsList = TouchPointsInterpol();
                GenerateMesh();
            }
        }

    }

    void Update()
    {
        GetTouch();

        if (!isDebug)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Input.mousePosition;

            //touchPointsList.Clear();
            touchPointsListTmp.Clear();
            prevMousePos = mousePos;

            Vector3 point = mousePos;
            point.z *= 0;

            //touchPointsList.Add(point);

            touchPointsListTmp.Add(point);
            return;
        }

        if (Input.GetMouseButton(0))
        {
            Vector3 mousePos = Input.mousePosition;

            Vector3 dir = mousePos - prevMousePos;
            float mag = dir.magnitude;

            if (mag >= distBtwMouseClick)
            {
                prevMousePos = mousePos;

                Vector3 point = mousePos;
                point.z *= 0;
                //touchPointsList.Add(point);
                touchPointsListTmp.Add(point);

                return;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if(touchPointsListTmp.Count < 6)
            {
                return;
            }
            touchPointsList = touchPointsListTmp;
            touchPointsList = TouchPointsInterpol();
            GenerateMesh();
        }
    }

    private void GenerateMesh()
    {
        verticiesList.Clear(); // ����� ��� ��� ��������� �����, ����� �� ���������.
        trianglesList.Clear();
        normalsList.Clear();


        GenerateVertices();
        GenerateTriangles();
        GenerateNormals();

        SetUpMesh();
        ChangeMeshPos();
    }

    private void SetUpMesh()
    {
        Vector3[] vertices = verticiesList.ToArray();
        int[] triangles = trianglesList.ToArray();
        Vector3[] normals = normalsList.ToArray();

        Mesh mesh = new Mesh();

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.normals = normals;


        meshFilter1.mesh = mesh;
        meshCollider1.sharedMesh = mesh;

        meshFilter2.mesh = mesh;
        meshCollider2.sharedMesh = mesh;

        OnMeshChanged?.Invoke();

    }

    private void ChangeMeshPos()
    {
        Vector3 vec1;

        vec1 = -maxVertexPos;
        MeshContainer1.transform.localPosition = new Vector3(-vec1.x * MeshContainer1.transform.localScale.x,
            vec1.y * MeshContainer1.transform.localScale.y, 0);

        MeshContainer2.transform.localPosition = new Vector3(MeshContainer1.transform.localPosition.x,
            MeshContainer1.transform.localPosition.y,
            MeshContainer2.transform.localPosition.z);
    }

    private void CheckMaxVertex()
    {
        Vector3 newVert = verticiesList[verticiesList.Count - 1];
        if (maxVertexPos.y < newVert.y)
        {
            maxVertexPos = newVert;
        }
    }

    private void GenerateVertices()
    {
        #region FirstVertices

        Vector3 perp1 = Vector2.Perpendicular(touchPointsList[1] - touchPointsList[0]).normalized;

        verticiesList.Add(new Vector3(touchPointsList[0].x + perp1.x * thickness,
            touchPointsList[0].y + perp1.y * thickness, 0));

        maxVertexPos = verticiesList[0];

        verticiesList.Add(new Vector3(touchPointsList[0].x - perp1.x * thickness,
            touchPointsList[0].y - perp1.y * thickness, 0));

        CheckMaxVertex();

        #endregion

        for (int k = 1; k < touchPointsList.Count - 1; k++)
        {
            CreateVertexPair(k);

            float angle = Vector3.Angle(touchPointsList[k] - touchPointsList[k + 1], touchPointsList[k] - touchPointsList[k - 1]);

            if (angle > 100)
            {
                continue;
            }

            Debug.Log(k);
            CreateVertexPair(k + 1);

            int i = verticiesList.Count - 1;

            Vector3 vec0 = verticiesList[i - 1] - verticiesList[i - 5];
            Vector3 vec1 = verticiesList[i] - verticiesList[i - 4];

            if (vec0.sqrMagnitude < vec1.sqrMagnitude)
            {

                verticiesList[i - 1] = verticiesList[i - 3];
                verticiesList[i - 5] = verticiesList[i - 3];
            }
            else
            {

                verticiesList[i] = verticiesList[i - 2];
                verticiesList[i - 4] = verticiesList[i - 2];
            }

            CheckMaxVertex();
            k++;

        }
        #region LastVertices
        Vector3 dir = touchPointsList[touchPointsList.Count - 1] - touchPointsList[touchPointsList.Count - 2]; //������ ����������
        Vector3 dirNormal = dir.normalized;

        Vector3 perp = Vector2.Perpendicular(dir).normalized;

        Vector3 resultVertex = new Vector3(touchPointsList[touchPointsList.Count - 1].x + perp.x * thickness,
                            touchPointsList[touchPointsList.Count - 1].y + perp.y * thickness, 0);

        verticiesList.Add(resultVertex);
        CheckMaxVertex();

        resultVertex = new Vector3(touchPointsList[touchPointsList.Count - 1].x - perp.x * thickness,
            touchPointsList[touchPointsList.Count - 1].y - perp.y * thickness, 0);

        verticiesList.Add(resultVertex);
        CheckMaxVertex();

        #endregion LastVertices

        #region Z vertices

        int verticeCount = verticiesList.Count;

        for(int i = 0; i < verticeCount; i++)
        {
            Vector3 resultVec = Vector3.forward * zThickness + verticiesList[i];
            verticiesList.Add(resultVec);
        }

        #endregion
    }


    private void GenerateTriangles()
    {
        int k = 0;

        int halfList = verticiesList.Count / 2;

        for (int i = 0; i < halfList - 3; i += 2)
        {
            k += 2;

            int i0 = i;
            int i1 = i + 1;
            int i2 = i + 2;
            int i3 = i + 3;

            int i4 = halfList + i;
            int i5 = halfList + i1;
            int i6 = halfList + i2;
            int i7 = halfList + i3;

            #region front quad
            trianglesList.Add(i0);
            trianglesList.Add(i2);
            trianglesList.Add(i1);

            trianglesList.Add(i1);
            trianglesList.Add(i2);
            trianglesList.Add(i3);
            #endregion

            #region back quad
            trianglesList.Add(i7);
            trianglesList.Add(i6);
            trianglesList.Add(i5);

            trianglesList.Add(i5);
            trianglesList.Add(i6);
            trianglesList.Add(i4);
            #endregion

            #region up quad
            trianglesList.Add(i4);
            trianglesList.Add(i6);
            trianglesList.Add(i0);

            trianglesList.Add(i0);
            trianglesList.Add(i6);
            trianglesList.Add(i2);
            #endregion

            #region down quad
            trianglesList.Add(i1);
            trianglesList.Add(i3);
            trianglesList.Add(i5);

            trianglesList.Add(i5);
            trianglesList.Add(i3);
            trianglesList.Add(i7);
            #endregion

        }


    }

    public void GenerateNormals()
    {
        for (int i = 0; i < verticiesList.Count / 2; i++)
        {
            normalsList.Add(Vector3.forward);
        }

        for (int i = verticiesList.Count / 2; i < verticiesList.Count; i++)
        {
            normalsList.Add(Vector3.forward);
        }
    }

    //TouchPointsInterpol
    private List<Vector3> TouchPointsInterpol()
    {
        List<Vector3> points = new List<Vector3>();

        float dist = 0; // i % 2 == 0 

        Vector3 lastVec = touchPointsList[0]; // i % 2 == 0 
        points.Add(touchPointsList[0]);

        for (int i = 1; i < touchPointsList.Count - 1; i++)
        {

            dist += (touchPointsList[i] - lastVec).magnitude;
            if (dist < distBtwPoints)
            {
                continue;
            }

            Vector3 resultVec = Vector3.Lerp(lastVec, touchPointsList[i], 1f);

            resultVec = (resultVec - lastVec).normalized * distBtwPoints + lastVec;

            points.Add(resultVec);
            lastVec = resultVec;

            dist = 0;
        }

        return points;
    }


    private void CreateVertexPair(int k)
    {
        Vector3 dir = touchPointsList[k + 1] - touchPointsList[k - 1];

        Vector3 perp = Vector2.Perpendicular(dir).normalized;
        //Vector3 perp = (touchPointsList[k] - verticiesList[verticiesList.Count - 2]).normalized;

        Vector3 mediana = (touchPointsList[k + 1] + touchPointsList[k - 1]) / 2 - touchPointsList[k];
        mediana = mediana.normalized;
        if(mediana.x == 0 || mediana.y == 0)
        {
            mediana = perp;
        }

        verticiesList.Add(new Vector3(touchPointsList[k].x + Mathf.Abs(mediana.x) * thickness * (perp.x >= 0 ? 1 : -1),
            touchPointsList[k].y + Mathf.Abs(mediana.y) * thickness * (perp.y >= 0 ? 1 : -1), 0));
        CheckMaxVertex();

        verticiesList.Add(new Vector3(touchPointsList[k].x + Mathf.Abs(mediana.x) * thickness * (perp.x >= 0 ? -1 : 1),
            touchPointsList[k].y + Mathf.Abs(mediana.y) * thickness * (perp.y >= 0 ? -1 : 1), 0));
        CheckMaxVertex();
        //Debug.Log("k = " + k + " ; v1 = " + verticiesList[verticiesList.Count - 2] + ", v2 = " + verticiesList[verticiesList.Count - 1]);

    }
}
