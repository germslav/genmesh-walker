
using UnityEngine;

public class IKControl : MonoBehaviour
{
    //protected Animator animator;

    public bool ikActive = false;
    public Transform rightLeg = null;
    public Transform leftLeg = null;

    public Animator animator;

    [Header("IK Movement")]
    public float offsetY;
    public float stepSize;
    public LayerMask layer;
    public float maxPoint;

    private Transform leftFoot;
    private Transform rightFoot;

    private Vector3 leftFootPos;
    private Vector3 rightFootPos;

    private Quaternion leftFootRotation;
    private Quaternion rightFootRotation;

    private float leftFootWeight;
    private float rightFootWeight;

    private float leftMag;
    private float rightMag;

    private Vector3 leftFootStartPos;
    private Vector3 rightFootStartPos;

    void Start()
    {
        //animator = GetComponent<Animator>();
        leftFoot = animator.GetBoneTransform(HumanBodyBones.LeftFoot);
        rightFoot = animator.GetBoneTransform(HumanBodyBones.RightFoot);

        leftFootRotation = leftFoot.rotation;
        rightFootRotation = rightFoot.rotation;


        leftFootStartPos = leftFoot.position;
        rightFootStartPos = rightFoot.position;
    }

    private void FixedUpdate()
    {
        RaycastHit leftHit;

        Vector3 Lpos = leftFoot.position;

        if (Physics.Raycast(Lpos, Vector3.down, out leftHit, 2, layer))
        {
            leftMag = leftHit.distance;

            leftFootPos = Vector3.Lerp(Lpos, leftHit.point + Vector3.up * offsetY + Vector3.right * stepSize, Time.fixedDeltaTime * 10f);
            leftFootRotation = Quaternion.FromToRotation(transform.up, leftHit.normal) * transform.rotation;
            Gizmos.color = Color.black;
            Debug.DrawLine(Lpos, leftFootPos);
        }

        RaycastHit rightHit;

        Vector3 Rpos = rightFoot.position;

        if (Physics.Raycast(Rpos, Vector3.down, out rightHit, 2, layer))
        {
            rightMag = rightHit.distance;

            rightFootPos = Vector3.Lerp(Rpos, rightHit.point + Vector3.up * offsetY + Vector3.right * stepSize, Time.fixedDeltaTime * 10f);
            rightFootRotation = Quaternion.FromToRotation(transform.up, rightHit.normal) * transform.rotation;
            Gizmos.color = Color.black;
            Debug.DrawLine(Rpos, rightFootPos);
        }

        float magL = (leftLeg.position - leftFootPos).magnitude;
        float magR = (rightLeg.position - rightFootPos).magnitude;

        float mag = Mathf.Max(magL, magR);
        offsetY = Mathf.Clamp(mag, 0.3f, 0.65f) * maxPoint;

    }


    void OnAnimatorIK()
    {
        leftFootWeight = animator.GetFloat("LeftFoot");
        rightFootWeight = animator.GetFloat("RightFoot");

        animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, leftFootWeight);
        animator.SetIKRotationWeight(AvatarIKGoal.LeftFoot, leftFootWeight);

        animator.SetIKPosition(AvatarIKGoal.LeftFoot, leftFootPos);
        animator.SetIKRotation(AvatarIKGoal.LeftFoot, leftFootRotation);

        animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, rightFootWeight);
        animator.SetIKRotationWeight(AvatarIKGoal.RightFoot, rightFootWeight);

        animator.SetIKPosition(AvatarIKGoal.RightFoot, rightFootPos);
        animator.SetIKRotation(AvatarIKGoal.RightFoot, rightFootRotation);

    }

    /*
    void OnAnimatorIK()
    {
        if (animator)
        {

            //if the IK is active, set the position and rotation directly to the goal. 
            if (ikActive)
            {

                // Set the look target position, if one has been assigned
                if (lookObj != null)
                {
                    animator.SetLookAtWeight(1);
                    animator.SetLookAtPosition(lookObj.position);
                }

                // Set the right hand target position and rotation, if one has been assigned
                if (rightLeg != null)
                {
                    animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, 1);
                    animator.SetIKRotationWeight(AvatarIKGoal.RightFoot, 1);
                    animator.SetIKPosition(AvatarIKGoal.RightFoot, rightLeg.position);
                    animator.SetIKRotation(AvatarIKGoal.RightFoot, rightLeg.rotation);
                }

                if (leftLeg != null)
                {
                    animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 1);
                    animator.SetIKRotationWeight(AvatarIKGoal.LeftFoot, 1);
                    animator.SetIKPosition(AvatarIKGoal.LeftFoot, leftLeg.position);
                    animator.SetIKRotation(AvatarIKGoal.LeftFoot, leftLeg.rotation);
                }
            }

            //if the IK is not active, set the position and rotation of the hand and head back to the original position
            else
            {
                animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.RightFoot, 0);
                animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.LeftFoot, 0);
                animator.SetLookAtWeight(0);
            }
        }
    }*/
}
