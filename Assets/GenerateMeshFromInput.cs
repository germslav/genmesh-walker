using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateMeshFromInput : MonoBehaviour
{
    public int n = 20;
    public float radius = 1f;
    public GameObject sphere;

    Vector3 prevPoint;

    List<Vector3> touchPointsList = new List<Vector3>();
    Vector3[] touchPoints;

    private bool isStartDraw = false;
    private int pointsCount = 0;

    Vector3[] vertices;
    Vector3[] normals;
    int[] triangles;

    List<Vector3> verticiesList = new List<Vector3>();
    List<int> trianglesList = new List<int>();
    List<Vector3> normalsList = new List<Vector3>();

    MeshFilter meshFilter;
    MeshRenderer meshRenderer;
    MeshCollider meshCollider;

    Mesh mesh;

    int o = 0;

    private void Awake()
    {
        meshFilter = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();
        meshCollider = GetComponent<MeshCollider>();
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Input.mousePosition;

            if (!isStartDraw)
            {
                isStartDraw = true;

                prevPoint = mousePos;

                Vector3 point = Vector3.zero;

                touchPointsList.Add(point);
                pointsCount = 1;
                return;
            }
        }

        if(Input.GetMouseButton(0))
        {
            Vector3 mousePos = Input.mousePosition;

            Vector3 dir = mousePos - prevPoint;
            float mag = dir.magnitude;
            if (mag > radius)
            {
                Debug.Log(mag);
                prevPoint = mousePos;
                Vector3 point = touchPointsList[pointsCount - 1] + radius / 3 * dir.normalized;
                touchPointsList.Add(point);
                pointsCount++;

                Debug.Log(pointsCount + ", " + touchPointsList[pointsCount - 1]);
                return;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            isStartDraw = false;
            touchPoints = touchPointsList.ToArray();


            foreach(Vector3 vec in touchPoints)
            {
                GenerateMesh(vec);
            }


            pointsCount = 0;

            CreateMesh();
        }
    }


    public void CreateMesh()
    {
        vertices = verticiesList.ToArray();
        triangles = trianglesList.ToArray();
        normals = normalsList.ToArray();

        mesh = new Mesh();
        mesh.vertices = vertices;

        mesh.triangles = triangles;
        mesh.normals = normals;
        meshFilter.mesh = mesh;

        meshCollider.sharedMesh = mesh;
    }

    public void GenerateMesh(Vector3 point)
    {
        GenerateVertices(point);
    }


    public void GenerateVertices(Vector3 point)
    {
        o++;
        var sp = Instantiate(sphere, transform);
        sp.transform.position = transform.position + new Vector3(point.x, point.y, 0);
        sp.name = (o).ToString();
        sp.SetActive(true);



        float x;
        float y;

        int vCount = verticiesList.Count + n;
        int vtmp = verticiesList.Count;

        for (int i = vtmp; i < vCount; i++)
        {
            x = point.x + radius * Mathf.Sin((2 * Mathf.PI * i) / n);
            y = point.y + radius * Mathf.Cos((2 * Mathf.PI * i) / n);
            verticiesList.Add(new Vector3(x, y, 0f));

            var sps = Instantiate(sphere, sp.transform);
            sps.transform.position = transform.position + new Vector3(x, y, 0);
            sps.transform.localScale /= 2;
            sps.name += "." + (i).ToString();
            sps.SetActive(true);
        }

        GenerateTriangles(vCount);
    }

    public void GenerateTriangles(int vCount)
    {
        int counter = 0;

        int ctmp = (trianglesList.Count/3)/pointsCount;
        int cttmp = vCount - n;

        for (int i = cttmp; i < vCount - 2; i++)
        {
            counter++;
            trianglesList.Add(cttmp);
            trianglesList.Add(i + 1);
            trianglesList.Add(i + 2);
            Debug.Log("triangle" + i + " : " + ctmp + " - " + (i + 1) + " -  " + (i+2));
        }

        Debug.Log("counter = " + counter + ", o = " + o + ", vCount = " + vCount + "cttmp = " + cttmp);


        GenerateNormals(vCount);
    }

    public void GenerateNormals(int vCount)
    {
        int ntmp = normalsList.Count;
        for (int i = ntmp; i < vCount; i++)
        {
            normalsList.Add(-Vector3.forward);
        }
    }
}
