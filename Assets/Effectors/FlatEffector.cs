using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlatEffector : Effector
{
    [SerializeField]
    private float speedFactor = 2f;

    Move move;
    InputMesh inputMesh;

    bool isEffecting = false;

    void Start()
    {
        inputMesh = GameObject.FindGameObjectWithTag("MeshGenerator").GetComponent<InputMesh>();
    }

    void Update()
    {

    }

    public override void Effect(GameObject affectedObject)
    {
        if (move == null)
        {
            move = affectedObject.GetComponent<Move>();
            Debug.Log(inputMesh);
        }

        List<Vector3> pointsList = inputMesh.touchPointsList;

        if(pointsList.Count <= 0)
        {
            return;
        }    

        float maxY = pointsList[0].y;
        float minY = pointsList[0].y;

        for (int i = 2; i < pointsList.Count; i += 2)
        {
            if (maxY < pointsList[i].y)
            {
                maxY = pointsList[i].y;
            }

            if (minY > pointsList[i].y)
            {
                minY = pointsList[i].y;
            }
        }

        float diffY = Mathf.Abs(maxY - minY);



        if (diffY < 100)
        {
            move.Speed = move.Speed * speedFactor;
        }

    }

    public override void StopEffect(GameObject affectedObject)
    {
        move.ResetSpeed();
    }

}
