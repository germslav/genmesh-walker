using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundEffector : Effector
{

    [SerializeField]
    private float speedFactor = 0.5f;

    private Move move;
    private InputMesh inputMesh;

    private MeshCollider meshCollider;

    bool isEffecting = false;


    private void Start()
    {
        inputMesh = GameObject.FindGameObjectWithTag("MeshGenerator").GetComponent<InputMesh>();
        meshCollider = gameObject.GetComponent<MeshCollider>();
    }

    public override void Effect(GameObject affectedObject)
    {
        if (move == null)
        {
            move = affectedObject.GetComponent<Move>();
            
        }

        List<Vector3> pointsList = inputMesh.touchPointsList;

        Vector3 mediana = (pointsList[pointsList.Count - 2] + pointsList[0]) / 2 - pointsList[pointsList.Count / 2];
        Debug.Log(mediana);

        StartCoroutine(DebuffDuration());

        if (mediana.y < 0)
        {
            move.Speed *= 2.5f;
            return;
        }

        move.Speed *= speedFactor;
        
    }

    public override void StopEffect(GameObject affectedObject)
    {
   
        StopCoroutine(DebuffDuration());

        move.ResetSpeed();
    }


    private void RecalculateEffect()
    {
        List<Vector3> pointsList = inputMesh.touchPointsList;
        Vector3 mediana = (pointsList[pointsList.Count - 2] + pointsList[0]) / 2 - pointsList[pointsList.Count / 2];
        if (mediana.y > 0)
        {
            move.ResetSpeed();
            move.Speed *= 2.5f;
        }
        else
        {
            move.ResetSpeed();
        }

        Debug.Log(mediana);
    }

    IEnumerator DebuffDuration()
    {
        Transform playerTransform = move.transform;
        float reculcPosX = transform.position.x + meshCollider.bounds.size.x / 5;
        while (true)
        {
            if(playerTransform.position.x >= reculcPosX)
            {
                RecalculateEffect();
                StopCoroutine(DebuffDuration());
            }
            else if(playerTransform.position.x > transform.position.x)
            {
                move.ResetSpeed();

            }

            
            yield return new WaitForSeconds(.1f);
        }
    }
}
