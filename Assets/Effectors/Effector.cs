using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Effector : MonoBehaviour
{
    public abstract void Effect(GameObject affectedObject);

    public abstract void StopEffect(GameObject affectedObject);
}

