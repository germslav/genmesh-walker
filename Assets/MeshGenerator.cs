
using System.Collections.Generic;

using UnityEngine;


public class MeshGenerator : MonoBehaviour
{
    public GameObject sphere;

    public int n = 20;
    public float radius = 1f;

    Vector3[] vertices = new Vector3[8];
    Vector2[] uv = new Vector2[8];
    int[] triangles = new int[36];

    Vector3[] normals;


    MeshFilter meshFilter;
    MeshRenderer meshRenderer;

    MeshCollider meshCollider;

    Mesh mesh;

    void Start()
    {
        meshFilter = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();
        meshCollider = GetComponent<MeshCollider>();


        //CreateBox();

        CreateBox2();

        mesh = new Mesh();
        mesh.vertices = vertices;
        //mesh.uv = uv;
        mesh.triangles = triangles;
        mesh.normals = normals;
        meshFilter.mesh = mesh;

        meshCollider.sharedMesh = mesh;



        for (int i = 0; i < vertices.Length; i++)
        {
            var sp = Instantiate(sphere, transform);
            sp.transform.localPosition = vertices[i];
            sp.name = i.ToString();
            sp.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GenerateVertices()
    {
        List<Vector3> verticiesList = new List<Vector3> { };
        float x;
        float y;
        for (int i = 0; i < n; i++)
        {
            x = radius * Mathf.Sin((2 * Mathf.PI * i) / n);
            y = radius * Mathf.Cos((2 * Mathf.PI * i) / n);
            verticiesList.Add(new Vector3(x, y, 0f));
        }
        vertices = verticiesList.ToArray();
    }

    public void GenerateTriangles()
    {
        List<int> trianglesList = new List<int> { };
        for (int i = 0; i < (n - 2); i++)
        {
            trianglesList.Add(0);
            trianglesList.Add(i + 1);
            trianglesList.Add(i + 2);
        }
        triangles = trianglesList.ToArray();

        Debug.Log(triangles.Length);
    }

    public void GenerateNormals()
    {
        List<Vector3> normalsList = new List<Vector3> { };
        for (int i = 0; i < vertices.Length; i++)
        {
            normalsList.Add(-Vector3.forward);
        }
        normals = normalsList.ToArray();
    }

    void CreateBox2()
    {

        GenerateVertices();
        GenerateTriangles();
        GenerateNormals();  
    }


    void CreateBox()
    {
        vertices[0] = new Vector3(0, 1, 0);
        vertices[1] = new Vector3(1, 1, 0);
        vertices[2] = new Vector3(0, 0, 0);
        vertices[3] = new Vector3(1, 0, 0);

        vertices[4] = new Vector3(0, 1, 1);
        vertices[5] = new Vector3(1, 1, 1);
        vertices[6] = new Vector3(0, 0, 1);
        vertices[7] = new Vector3(1, 0, 1);

        uv[0] = new Vector2(0, 1);
        uv[1] = new Vector2(1, 1);
        uv[2] = new Vector2(0, 0);
        uv[3] = new Vector2(1, 0);

        uv[4] = new Vector2(1, 0);
        uv[5] = new Vector2(0, 0);
        uv[6] = new Vector2(1, 1);
        uv[7] = new Vector2(0, 1);


        //�����
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 1;
        triangles[5] = 3;

        //zad
        triangles[6] = 7;
        triangles[7] = 5;
        triangles[8] = 6;
        triangles[9] = 6;
        triangles[10] = 5;
        triangles[11] = 4;

        //levo
        triangles[12] = 4;
        triangles[13] = 0;
        triangles[14] = 2;
        triangles[15] = 2;
        triangles[16] = 6;
        triangles[17] = 4;

        //pravo
        triangles[18] = 1;
        triangles[19] = 5;
        triangles[20] = 3;
        triangles[21] = 3;
        triangles[22] = 5;
        triangles[23] = 7;

        //niz
        triangles[24] = 2;
        triangles[25] = 3;
        triangles[26] = 6;
        triangles[27] = 6;
        triangles[28] = 3;
        triangles[29] = 7;

        //verh
        triangles[30] = 4;
        triangles[31] = 5;
        triangles[32] = 0;
        triangles[33] = 0;
        triangles[34] = 5;
        triangles[35] = 1;
    }
}
